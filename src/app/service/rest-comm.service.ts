import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RestCommService {
  API_URL="http://localhost";
  GET_CRED_LIST_URL="getcredlist";
  CREDENTIAL_CRUD_URL="creds";
  

  constructor(private http: HttpClient) { }
  
  getEmailList(){

    return this.http.get<any>(this.API_URL+'/'+this.GET_CRED_LIST_URL);
  };
  saveUserCred(data){
    return this.http.post<any>(this.API_URL+"/"+this.CREDENTIAL_CRUD_URL,data);
  };

  updateUserCred(data){

    return this.http.put<any>(this.API_URL+"/"+this.CREDENTIAL_CRUD_URL,data);
  }
  deleteCred(id) : Observable<any>{

    return this.http.delete(this.API_URL+'/'+this.CREDENTIAL_CRUD_URL+"?id="+id);
  }
}
