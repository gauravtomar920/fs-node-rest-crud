const express = require("express");
const morgan = require("morgan");
const router = require("./router");
const accessControl = require('./middlewares/accessControl.middleware');
const errorHandler = require('./middlewares/errorHandler.middleware');
const bodyParser = require("body-parser");

app = express();app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(accessControl.settings);
app.use(morgan('dev'));

app.use(router);
app.use(errorHandler.catchError);

module.exports = app;
